import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _textController = TextEditingController();
  var _helloText = 'Seja bem vindo, Luiz!';

  void _onPressElevatedButton() {
    setState(() {
      _helloText = 'Seja bem vindo, ${_textController.text}';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Meu App - Aula 01'),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => {},
          backgroundColor: Colors.blue.shade200,
          child: const Icon(
            Icons.download,
            color: Colors.red,
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('Hello World'),
            Text(_helloText),
            Padding(
              padding: const EdgeInsets.all(20),
              child: TextField(controller: _textController),
            ),
            ElevatedButton(
                onPressed: _onPressElevatedButton,
                child: const Text('Click here'))
          ],
        ));
  }
}
